using UnityEngine;

public class pressingButton : MonoBehaviour
{
    public bool isHeldDown = false;
 
    public void onPress ()
    {
        isHeldDown = true;
        Debug.Log(isHeldDown);
    }
 
    public void onRelease ()
    {
        isHeldDown = false;
        Debug.Log(isHeldDown);
    }
}
