using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private GameObject _nameInput; // Input donde escribimos
    private GameObject _nameOnText; // 
    private InputField _input;
    private Text _nameOnInput;
    // Start is called before the first frame update
    void Start()
    {
        _nameOnText = GameObject.Find("playerName");
        
        _nameInput = GameObject.Find("NameInput");

        GetComponent<Button>().onClick.AddListener(LoadGame);
    }
    
    void LoadGame()
    {
        _input = _nameInput.GetComponent<InputField>();
        
        Debug.Log("Vamos a comprobar el texto: ["+_input.text+"]");
        
        _nameOnInput = _nameOnText.GetComponent<Text>();

        _nameOnInput.text = _input.text;
        
        SceneManager.LoadScene("GameScene");
    }
}
