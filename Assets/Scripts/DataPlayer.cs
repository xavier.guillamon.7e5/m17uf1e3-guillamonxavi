using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DataPlayer : MonoBehaviour
{
    public new string name;
    private WalkingState _statusPlayer = WalkingState.Quiet;
    public PlayerClass job;
    private GiantLevel _giantLevel = GiantLevel.MinLevel;
    public float height;
    public float weight;
    public float speed;
    public float distance;
    public Sprite[] walkingMove;
    public enum PlayerClass
    {
        Apothecary,
        Merchant,
        Scholar,
        Dancer,
        Thief
    }
    public enum WalkingState
    {
        Walking,
        Quiet
    }

    public enum GiantLevel
    {
        Increasing,
        MaxLevel,
        Decreasing,
        MinLevel
    }
    public Button Gigantomaquia;
    
    private SpriteRenderer _spriteRenderer;
    private int _counter = 0;
    private float _timeCounterAnimation = 0f;
    private float _timeBetweenFrames = 1f/12;
    private float _timeCounterQuiet = 0f;
    private const float _timeBetweenWalking = 2f;
    private Vector3 _velocity;
    private float _distanceCounter = 0;
    private float _increasingTime = 2f;
    private float _timeCounterIncreasing = 0f;
    private float _reachMaxHeight = 2f;
    private float _timeCounterMaxLevel = 0f;
    private float _reachMinHeight = 2f;
    private float _timeCounterMinLevel = 0f;

    // Start is called before the first frame update
    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        _velocity = new Vector3(speed/weight, 0, 0);
        _spriteRenderer.sprite = walkingMove[_counter++];
    }

    // Update is called once per frame
    void Update()
    {
        switch (_statusPlayer)
        {
            case WalkingState.Quiet:
                UpdateQuiet();
                break;
            case WalkingState.Walking:
                UpdateAnimation();
                UpdateWalking();
                break;
        }

        switch (_giantLevel)
        {
            case GiantLevel.Increasing:
                UpdateIncreasingGiantLevel();
                break;
            case GiantLevel.Decreasing:
                UpdateDecreasingGiantLevel();
                break;
            case GiantLevel.MaxLevel:
                UpdateMaxLevelGiant();
                break;
            case GiantLevel.MinLevel:
                UpdateMinLevelGiant();
                break;
        }
    }

    void UpdateAnimation()
    {
        _timeCounterAnimation += Time.deltaTime;
        if (_timeCounterAnimation > _timeBetweenFrames)
        {
            _timeCounterAnimation = 0;
            _spriteRenderer.sprite = walkingMove[_counter++];
            if (_counter >= walkingMove.Length) _counter = 0;
        }
    }

    void UpdateQuiet()
    {
        _timeCounterQuiet += Time.deltaTime;
        if (_timeCounterQuiet > _timeBetweenWalking)
        {
            _timeCounterQuiet = 0f;
            _statusPlayer = WalkingState.Walking;
            GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        }
    }
    void UpdateWalking()
    {
        GetComponent<Transform>().position += _velocity * Time.deltaTime;
        _distanceCounter += _velocity.magnitude * Time.deltaTime;
        if (_distanceCounter > distance)
        {
            _distanceCounter = 0f;
            _counter = 0;
            _spriteRenderer.sprite = walkingMove[_counter++];
            _statusPlayer = WalkingState.Quiet;
            _velocity = -_velocity;
        }
    }

    void UpdateIncreasingGiantLevel()
    {
        _timeCounterIncreasing += Time.deltaTime;
        float scale = 1 + ((height-1) / _increasingTime) * _timeCounterIncreasing ;
        GetComponent<Transform>().localScale = new Vector3(scale, scale, 1);
        if (_timeCounterIncreasing > _increasingTime)
        {
            _timeCounterIncreasing = 0f;
            _giantLevel = GiantLevel.MaxLevel;
        }
        Debug.Log(height + " - " + _increasingTime + " - " + _timeCounterIncreasing);
    }
    void UpdateDecreasingGiantLevel()
    {
        _timeCounterIncreasing += Time.deltaTime;
        float scale = 1 + ((height-1) / _increasingTime) * (_increasingTime-_timeCounterIncreasing);
        GetComponent<Transform>().localScale = new Vector3(scale, scale, 1);
        if (_timeCounterIncreasing > _increasingTime)
        {
            _timeCounterIncreasing = 0f;
            _giantLevel = GiantLevel.MinLevel;
            
        }
    }

    void UpdateMaxLevelGiant()
    {
        _timeCounterMaxLevel += Time.deltaTime;
        if (_timeCounterMaxLevel > _reachMaxHeight)
        {
            _timeCounterMaxLevel = 0f;
            _giantLevel = GiantLevel.Decreasing;
        }
    }
    void UpdateMinLevelGiant()
    {
        _timeCounterMinLevel += Time.deltaTime;
        if (Gigantomaquia.GetComponent<pressingButton>().isHeldDown && _timeCounterMinLevel > _reachMinHeight)
        {
            _timeCounterMinLevel = 0f;
            _giantLevel = GiantLevel.Increasing;
        }
    }
}
